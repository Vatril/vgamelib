package com.vatril.vgamelib;

/**
 * Created by vatril on 07.11.16.
 */


/**
 * Strings that are used throughout the library
 */
public final class Strings {

    private Strings() {}

    /**
     * String that is but in front of Debugging Info
     */
    public final static String VGLDebug = "VGameLib Debugger";

    /**
     * String that is but in front of Errors
     */
    public final static String VGLError = "VGameLib ERROR";


    /**
     * Name of the shared preferences
     */
    public final static String VGLPrefs = "VGameLibPrefs";

    /**
     * Name of the preference if HQ images should be used
     */
    public final static String VGLPrefImageHQ = "VGameLib_ImageIsHQ";
}
