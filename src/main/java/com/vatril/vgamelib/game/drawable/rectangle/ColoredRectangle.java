package com.vatril.vgamelib.game.drawable.rectangle;

/**
 * Created by vatril on 21.11.16.
 */

public class ColoredRectangle extends Rectangle {

    public ColoredRectangle(int a, int r, int g, int b) {
        setColor(a,r,g,b);
    }

    public ColoredRectangle(int a, int r, int g, int b, int w, int h) {
        setSize(w,h);
        setColor(a,r,g,b);
    }

    public ColoredRectangle(int a, int r, int g, int b, int w, int h, int x, int y) {
        setPosition(x,y);
        setSize(w,h);
        setColor(a,r,g,b);
    }
}
