package com.vatril.vgamelib.game.drawable.rectangle;

/**
 * Created by Vatril on 09/12/16.
 */

public class Hitbox extends Rectangle {
    private float xOff= 0,yOff = 0;

    public Hitbox(float xOff, float yOff, float w, float h) {
        this.xOff = xOff;
        this.yOff = yOff;
        setSize(w,h);
        setPosition(xOff,yOff);
    }

    public void updatePosition(float x, float y) {;
        setPosition(x + xOff, y + yOff);
    }

}

