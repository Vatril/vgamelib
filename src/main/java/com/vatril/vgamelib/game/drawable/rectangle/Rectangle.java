package com.vatril.vgamelib.game.drawable.rectangle;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.vatril.vgamelib.game.drawable.Drawable;

/**
 * Created by vatril on 21.11.16.
 */


/**
 * Basic Rectangle, that can be drawn on a canvas.
 */
public abstract class Rectangle implements Drawable {

    private float x,y,w,h;
    private Paint paint;


    /**
     * Basic Rectangle, that can be drawn on a canvas.
     * Default constructor, all values 0
     */
    public Rectangle() {
        paint = new Paint();
        x = 0;
        y = 0;
        w = 0;
        h = 0;
    }



    /**
     * sets the X coordinate of the Rectangle
     * @param x the X coordinate
     */
    public void setX(float x) {
        this.x = x;
    }


    /**
     * sets the Y coordinate of the Rectangle
     * @param y the Y coordinate
     */
    public void setY(float y) {
        this.y = y;
    }


    /**
     * sets the width of the Rectangle
     * @param w the width
     */
    public void setWidth(float w) {
        this.w = w;
    }


    /**
     * sets the height of the Rectangle
     * @param h the height
     */
    public void setHeight(float h) {
        this.h = h;
    }

    /**
     * sets the position of the Rectangle
     * @param x the X coordinate
     * @param y the Y coordinate
     */
    public void setPosition(float x, float y){
        setX(x);
        setY(y);
    }


    /**
     * sets the size of the Rectangle
     * @param w the width
     * @param h the height
     */
    public void setSize(float w, float h){
        setWidth(w);
        setHeight(h);
    }


    /**
     * sets the ARGB value of the color of the Rectangle
     *
     * @param a the alpha
     * @param r the red
     * @param g the green
     * @param b the blue
     */
    public void setColor(int a, int r, int g, int b){
        paint.setARGB(a,r,g,b);
    }


    /**
     * sets the color of the Rectangle
     * @param color the color of the Rectangle
     */
    public void setColor(int color){
        paint.setColor(color);
    }


    /**
     * sets the paint of the Rectangle
     * @param paint the paint
     */
    public void setPaint(Paint paint) {
        this.paint = paint;
    }


    /**
     * gets the paint of the Rectangle
     * @return the paint
     */
    public Paint getPaint() {
        return paint;
    }


    /**
     * gets the height of the Rectangle
     * @return the height
     */
    public float getHeight() {
        return h;
    }


    /**
     * gets the width of the Rectangle
     * @return the width
     */
    public float getWidth() {
        return w;
    }


    /**
     * gets the Y coordinate of the Rectangle
     * @return the Y coordinate
     */
    public float getY() {
        return y;
    }


    /**
     * gets the X coordinate of the Rectangle
     * @return the X coodinate
     */
    public float getX() {
        return x;
    }


    /**
     * Rectangle is drawn on the canvas using its Position, Size and Paint.
     * @param canvas that is drawn on
     */
    @Override
    public void draw(Canvas canvas) {
        canvas.drawRect(x,y,x+w,y+h,paint);
    }
}
