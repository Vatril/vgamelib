package com.vatril.vgamelib.game.drawable;

import android.graphics.Canvas;

/**
 * Created by vatril on 21.11.16.
 */


/**
 * Can be drawn on a canvas
 */
public interface Drawable {


    /**
     * In this method the drawable is drawn on the canvas
     * @param canvas that is drawn on
     */
    void draw(Canvas canvas);
}
