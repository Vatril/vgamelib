package com.vatril.vgamelib.game;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.vatril.vgamelib.Gamemanager;
import com.vatril.vgamelib.game.gameobject.Gameobject;
import com.vatril.vgamelib.game.interaction.Touch;
import com.vatril.vgamelib.game.interaction.Touchable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by vatril on 19.11.16.
 */


/**
 * The game is the container where all the drawables are rendered
 */
public class Game implements Runnable {

    private boolean isRunning;
    private Thread thread;
    private HashSet<Gameobject> gameobjects;
    private HashSet<Touchable> touches;
    private HashSet<Gameobject> toAddGameobjects;
    private HashSet<Gameobject> toRemoveGameobjects;
    private GameSurface gameSurface;
    private int width = 0, height = 0;
    private float scale = 1;


    /**
     * Initializes the game
     * @param gameSurface where the game should be rendered
     */
    public Game(GameSurface gameSurface){
        this.gameSurface = gameSurface;

        gameobjects = new HashSet<>();
        toAddGameobjects = new HashSet<>();
        toRemoveGameobjects = new HashSet<>();
        touches = new HashSet<>();
        start();
    }

    /**
     *
     * @return the height of the canvas
     */
    public int getHeight() {
        return height;
    }

    /**
     *
     * @return the width of the canvas
     */
    public int getWidth() {
        return width;
    }


    /**
     *
     * @return if the game is currently running
     */
    public boolean isRunning() {
        return isRunning;
    }


    /**
     * stops the game
     */
    private void stop(){
        if(!isRunning)
            return;
        isRunning = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * starts the game
     */
    private void start(){
        if (isRunning)
            return;
        thread = new Thread(this);
        thread.start();
    }


    /**
     * Adds one or more gameobjects to the game
     * @param gameobjects one or more gameobjects
     * @return the game
     */
    public Game addGameobject(Gameobject...gameobjects){
        Collections.addAll(toAddGameobjects, gameobjects);
        return this;
    }

    /**
     * Removes one or more gameobjects from the game
     * @param gameobjects one or more gameobjects
     * @return the game
     */
    public Game removeGameobject(Gameobject...gameobjects){
        Collections.addAll(toRemoveGameobjects, gameobjects);
        return this;
    }


    /**
     * the main gameloop
     */
    @Override
    public void run() {
        isRunning = true;

        Canvas canvas;
        double fps = Gamemanager.getFPS();
        double timePerTick = 1000000000 / fps;
        long lastTime = System.nanoTime();
        long currentTime;
        long counter = 0;
        Paint p = new Paint();
        int i = 0;
        ArrayList<Gameobject> tempList = new ArrayList<>();

        while (isRunning){

            if (!gameSurface.getHolder().getSurface().isValid())
                continue;

            currentTime = System.nanoTime();
            counter += currentTime - lastTime;

            lastTime = currentTime;

            if(counter >= timePerTick){
                canvas = gameSurface.getHolder().lockCanvas();

                width = canvas.getWidth();
                height = canvas.getHeight();

                if (width == 0 || height == 0)
                    continue;


                scale = (float) width/1920f;
                tempList.clear();
                i = 0;

                Iterator<Gameobject> remItt = toRemoveGameobjects.iterator();
                while (remItt.hasNext()){
                    Gameobject gameobject = remItt.next();
                    gameobjects.remove(gameobject);
                    remItt.remove();
                    gameobject.destroy();
                }

                Iterator<Gameobject> addItt = toAddGameobjects.iterator();
                while (addItt.hasNext()){
                    Gameobject gameobject = addItt.next();
                    gameobjects.add(gameobject);
                    addItt.remove();
                    gameobject.init(this);
                }

                canvas.drawRGB(255,255,255);

                i = 0;
                for (Gameobject g : gameobjects) {
                    Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    Canvas tempCanvas = new Canvas(bmp);
                    g.tick(i);
                    g.render(tempCanvas);
                    canvas.drawBitmap(Bitmap.createScaledBitmap(bmp,(int)(bmp.getWidth() * scale),(int)(bmp.getHeight() * scale),false), g.getX() * scale, g.getY() * scale, null);
                    i++;
                }


                for (Touch t : gameSurface.getTouches()) {
                    if (t == null)
                        continue;
                    tempList.clear();
                    for (Gameobject g: gameobjects){
                        if (t.getIndex() == g.getDragIndex()){
                            g.setPosition(t.getX() - g.getHitbox().getWidth()/2,t.getY() - g.getHitbox().getHeight()/2);
                        }
                        if (g.hitsPoint(t.getX()/scale,t.getY()/scale))
                            tempList.add(g);
                    }
                    if (tempList.size() > 1){
                        Gameobject tempObj = null;
                        int indexTest = -1;
                        for (Gameobject ob : tempList){
                            if (ob.getIndex() > indexTest) {
                                tempObj = ob;
                                indexTest = ob.getIndex();
                            }
                        }
                        if (tempObj != null)
                            if(touches.contains(tempObj)){
                                tempObj.move(t);
                            }else {
                                touches.add(tempObj);
                                tempObj.touch(t);
                            }
                    }else if(tempList.size() == 1){
                        Gameobject g = tempList.get(0);
                        if(touches.contains(g)){
                            g.move(t);
                        }else {
                            touches.add(g);
                            g.touch(t);
                        }
                    }
                }

                Iterator<Touchable> tItt= touches.iterator();
                while (tItt.hasNext()){
                    Touchable t = tItt.next();
                    if (!t.wasTouched()){
                        tItt.remove();
                        t.release();
                    }
                }

                if (Gamemanager.isDebugging()){
                    double realfps = (double) 1000000000/counter;
                    @SuppressLint("DefaultLocale") String fpsToString = String.format("%.2f",realfps);
                    @SuppressLint("DefaultLocale") String desfpsToString = String.format("%.2f",fps);
                    //@SuppressLint("DefaultLocale") String fpsToPercent = String.format("%.2f",(100.0/ (double) fps) * realfps);
                    String fpsFinalString = "FPS: " +  fpsToString + "/" + desfpsToString/* + " (" + fpsToPercent + "%)"*/;
                    p.setTextSize(50);
                    p.setARGB(255,0,0,0);
                    p.setStrokeWidth(8);
                    p.setStyle(Paint.Style.STROKE);
                    canvas.drawText(fpsFinalString,10,60,p);
                    p.setARGB(255,255,255,0);
                    p.setStyle(Paint.Style.FILL);
                    canvas.drawText(fpsFinalString,10,60,p);
                }
                gameSurface.getHolder().unlockCanvasAndPost(canvas);


                counter = 0;
            }

        }
        stop();
    }
}
