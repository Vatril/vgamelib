package com.vatril.vgamelib.game;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.vatril.vgamelib.game.interaction.Touch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by vatril on 19.11.16.
 */

/**
 * The GameSurface holds the canvas th game runs on
 */
public class GameSurface extends SurfaceView implements SurfaceHolder.Callback {

    private Touch[] touches;



    public GameSurface(Context context) {
        super(context);
        init();
    }

    public GameSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GameSurface(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    /**
     * Initializes everything necessary
     */
    private void init(){
        touches = new Touch[10];
    }


    /**
     * an Array with all possible touches
     * touch is null, if there wasn't one with that index
     * @return Touch[] of all possible touches
     */
    public Touch[] getTouches() {
        return touches;
    }


    /**
     * Handles Touches
     * @param event
     * @return true
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {


        for(int i = 0; i < event.getPointerCount(); i++){

            if(event.getPointerId(i) != MotionEvent.INVALID_POINTER_ID) {
                removeTouch(i);

                if ((event.getAction() & MotionEvent.ACTION_MASK) != MotionEvent.ACTION_POINTER_UP && (event.getAction() & MotionEvent.ACTION_MASK) != MotionEvent.ACTION_UP
                        && (event.getAction() & MotionEvent.ACTION_MASK) != MotionEvent.ACTION_CANCEL)
                    addTouch(new Touch(event.getX(i), event.getY(i), i));

            }
        }

        return true;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }


    /**
     * adds a Touch to the touchlist
     * overrides the current touchevent with the same index
     * @param touch
     */
    private void addTouch(Touch touch){
        if (touch.getIndex()<10)
            touches[touch.getIndex()] = touch;
    }

    /**
     * removes a touch with index
     * @param index
     */
    private void removeTouch(int index){
        if (index<10)
            touches[index] = null;
    }
}
