package com.vatril.vgamelib.game.gameobject;

import android.graphics.Canvas;
import android.graphics.Rect;

import com.vatril.vgamelib.Gamemanager;
import com.vatril.vgamelib.game.Game;
import com.vatril.vgamelib.game.Rendable;
import com.vatril.vgamelib.game.drawable.rectangle.Hitbox;
import com.vatril.vgamelib.game.drawable.rectangle.Rectangle;
import com.vatril.vgamelib.game.interaction.Touch;
import com.vatril.vgamelib.game.interaction.Touchable;

/**
 * Created by Vatril on 09/12/16.
 */


/**
 * A Gameobject can be put in a Game
 */
public abstract class Gameobject implements Rendable, Touchable {

    private Hitbox hitbox;
    private Game game;
    private float x = 0,y = 0;
    private int index = -1, releaseSmoother = 0, drag = -1;
    private boolean wasTouched = false;


    /**
     * Gets called when the gameobjects added to the game
     * @param game the game the runnable is running in
     */
    @Override
    public final void init(Game game) {
        this.game = game;
        onInit();
    }


    /**
     * Gets called when thegame ticks
     * @param index
     */
    @Override
    public final void tick(int index) {
        wasTouched = false;
        if (releaseSmoother > 0)
            releaseSmoother++;
        if (releaseSmoother > 2){
            releaseSmoother = 0;
            onRelease();
        }

        this.index = index;
        if (hitbox != null)
            hitbox.updatePosition(x,y);


        onTick();
    }


    /**
     * Gets called when the game renders
     * @param canvas that will be drawn on
     */
    @Override
    public final void render(Canvas canvas) {
        onRender(canvas);
    }

    /**
     * Gets called when the gameobject gets removed from the game
     */
    @Override
    public final void destroy() {
        onDestroy();
    }

    /**
     * Gets called when the gameobject gets touched
     * @param touch
     */
    @Override
    public final void touch(Touch touch) {
        wasTouched = true;
        onTouch(touch);
    }

    /**
     * Gets called when the gameobject gets released
     */
    @Override
    public final void release() {
        releaseSmoother = 1;
        wasTouched = false;
    }

    /**
     * Gets called when the gameobject gets moved
     * @param touch
     */
    @Override
    public final void move(Touch touch) {
        wasTouched = true;
        onMove(touch);
    }


    /**
     *
     * @return rectangle that represents the hitbox
     */
    @Override
    public Rectangle getHitbox() {
        return hitbox;
    }

    /**
     * Starts the drag where the objects follows the finger with the given index
     * @param index
     */
    public void startDrag(int index){
        drag = index;
    }

    /**
     * stops the drag
     */
    public void stopDrag(){
        drag = -1;
    }

    /**
     * Gets called when the gameobject added tho the game
     */
    protected abstract void onInit();

    /**
     * Gets called when the game ticks
     */
    protected abstract void onTick();

    /**
     * Gets called when the game renders
     * @param canvas where the object is drawn on
     */
    protected abstract void onRender(Canvas canvas);

    /**
     * gets called when the object gets destroyed
     */
    protected abstract void onDestroy();

    /**
     * Gets called when the gameobject gets touched
     * @param touch
     */
    protected abstract void onTouch(Touch touch);

    /**
     * Gets called when the gameobject gets released
     */
    protected abstract void onRelease();

    /**
     * Gets called when the gameobject gets moved
     * @param touch
     */
    protected abstract void onMove(Touch touch);

    /**
     *
     * @return the game the object is on
     */
    protected Game getGame(){
        return game;
    }

    /**
     * sets the hitbox of the gameobject
     * @param hitbox
     */
    protected void setHitbox(Hitbox hitbox){
        this.hitbox = hitbox;
    }

    /**
     * sets the x coordinate of the gameobject
     * @param x
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * sets the y coordinate of the gameobject
     * @param y
     */
    public void setY(float y) {
        this.y = y;
    }


    /**
     * sets the position of the gameobject
     * @param x
     * @param y
     */
    public void setPosition(float x, float y){
        setX(x);
        setY(y);
    }

    /**
     *
     * @return the x coordinate of the gameobject
     */
    public float getX() {
        return x;
    }

    /**
     *
     * @return the y coordinate of the gameobject
     */
    public float getY() {
        return y;
    }

    /**
     * gets the index of the gameobject
     * @return index
     */
    public int getIndex() {
        return index;
    }

    /**
     *
     * @return if the gameobject was touched during the last check
     */
    @Override
    public boolean wasTouched() {
        return wasTouched;
    }

    /**
     * checks if the gameobject collides with another gameobject
     * @param test
     * @return if they collied
     */
    public boolean hitsGameobject(Gameobject test){
        if (test.hitbox == null || this.hitbox == null)
            return false;

        return new Rect((int)test.hitbox.getX(),(int)test.hitbox.getY(),(int)(test.hitbox.getWidth() + test.hitbox.getX()),(int)(test.hitbox.getHeight() + test.hitbox.getY())).contains(
                new Rect((int)hitbox.getX(),(int)hitbox.getY(),(int)(hitbox.getWidth() + hitbox.getX()),(int)(hitbox.getHeight() + hitbox.getY()))
        );
    }

    /**
     * checks if a point is in a gameobject
     * @param x
     * @param y
     * @return if it lays in the gameobject
     */
    public boolean hitsPoint(float x, float y){
        if (this.hitbox == null)
            return false;
       return new Rect((int)hitbox.getX(),(int)hitbox.getY(),(int)(hitbox.getWidth() + hitbox.getX()),(int)(hitbox.getHeight() + hitbox.getY())).contains((int) (x),(int) (y));
    }

    /**
     * gets the index the gameobject should follow
     * @return the index
     */
    public int getDragIndex() {
        return drag;
    }
}
