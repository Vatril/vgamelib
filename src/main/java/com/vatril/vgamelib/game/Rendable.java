package com.vatril.vgamelib.game;

import android.graphics.Canvas;

/**
 * Created by vatril on 19.11.16.
 */


/**
 *
 */
public interface Rendable {


    /**
     * This method is called when the rendable is added to the game
     *
     * @param game the game the runnable is running in
     */
    void init(Game game);


    /**
     * This method is called before rendering
     * All calculations should be done here
     *
     */
    void tick(int index);


    /**
     * This method is called when frame is drawn on the screen
     *
     * @param canvas that will be drawn on
     */
    void render(Canvas canvas);


    /**
     * This method is called when the rendable is removed from the game
     *
     */
    void destroy();
}
