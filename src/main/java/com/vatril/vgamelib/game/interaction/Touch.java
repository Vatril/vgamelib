package com.vatril.vgamelib.game.interaction;

/**
 * Created by Vatril on 08/12/16.
 */


/**
 * A touch holds its position and index
 */
public class Touch {
    private float x,y;
    private int index;


    /**
     *
     * @param x for the x coordinate
     * @param y for the y coordinate
     * @param index for the index of the finger touching
     */
    public Touch(float x, float y, int index){
        this.x = x;
        this.y = y;
        this.index = index;
    }


    /**
     *
     * @return the x coordinate of the touch
     */
    public float getX() {
        return x;
    }

    /**
     *
     * @return the y coordinate of the touch
     */
    public float getY() {
        return y;
    }


    /**
     *
     * @return the index of the finger touching
     */
    public int getIndex() {
        return index;
    }


    /**
     * returns true if the two touches have the same values
     * @param obj the touch that should be compared
     * @return true if the two touches have the same values
     */
    @Override
    public boolean equals(Object obj){
        if (obj instanceof Touch){
            Touch st = (Touch) obj;
            if (x == st.x && y == st.y && index == st.index){
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return a copy of the touch
     */
    @Override
    public Touch clone(){
        return new Touch(x,y,index);
    }
}
