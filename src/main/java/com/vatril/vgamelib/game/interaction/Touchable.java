package com.vatril.vgamelib.game.interaction;

import com.vatril.vgamelib.game.drawable.rectangle.Rectangle;

/**
 * Created by Vatril on 09/12/16.
 */


/**
 * Makes something Touchable
 */
public interface Touchable {

    /**
     * is called when its touched
     * @param touch
     */
    void touch(Touch touch);

    /**
     * is called when its released
     */
    void release();

    /**
     * is called when its moved
     * @param touch
     */
    void move(Touch touch);

    /**
     *
     * @return the rectangle which represent the hitbox
     */
    Rectangle getHitbox();

    /**
     *
     * @return if itwas touched during the last check
     */
    boolean wasTouched();
}
