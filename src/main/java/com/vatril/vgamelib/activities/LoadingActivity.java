package com.vatril.vgamelib.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vatril.vgamelib.Gamemanager;
import com.vatril.vgamelib.R;
import com.vatril.vgamelib.Strings;
import com.vatril.vgamelib.assets.Assets;
import com.vatril.vgamelib.util.ActivityUtil;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * Created by vatril on 08.11.16.
 */


/**
 *Activity with a loading screen
 *can hold a image
 */
public abstract class LoadingActivity extends Activity {

    private ProgressBar progressBar;
    private TextView text;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUtil.setUpFullScreenActivity(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.loading_screen);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        text = (TextView) findViewById(R.id.textDisplay);
        image = (ImageView) findViewById(R.id.imageView);
        ImageView logo = (ImageView) findViewById(R.id.logo);
        logo.setImageResource(R.drawable.logosmall);
        progressBar.setProgress(0);
        text.setText("");
    }


    /**
     * loads ether high or low res assets into the assets class
     *
     * @param resources the resources of the current application
     * @param rDotDrawable R.drawable class of the current application
     * @param context of the current application
     * @param hqImages ids of high res image resources
     * @param lqImage ids of low res image resources
     */
    public final void loadAssets(android.content.res.Resources resources, Class rDotDrawable, Context context, int[] hqImages, int[] lqImage){

        SharedPreferences pref = context.getSharedPreferences(Strings.VGLPrefs,MODE_PRIVATE);

        int [] images;

        if (pref.getBoolean(Strings.VGLPrefImageHQ,false))
            images = hqImages;
        else
            images = lqImage;

        Field[] fields = rDotDrawable.getFields();
        progressBar.setMax(images.length);

        HashMap<String,Bitmap> imagesMap = new HashMap<>();

        int counter = 0;

        for (int i = 0; i < fields.length; i++) {
            for (int j = 0; j < images.length; j++) {
                try{
                    if (fields[i].getInt(null) == images[j]){
                        imagesMap.put(fields[i].getName(), BitmapFactory.decodeResource(resources,images[j]));
                        text.setText(fields[i].getName().replace("_"," "));
                        progressBar.setProgress(progressBar.getProgress() + 1);
                        counter++;
                        if (Gamemanager.isDebugging())
                            System.out.println(Strings.VGLDebug + ": Loading " +fields[i].getName().replace("_"," "));
                        if (counter == images.length)
                            break;
                    }
                }catch (IllegalAccessException iae){
                    System.err.println(Strings.VGLError +": Error loading " + fields[i].getName());
                }
            }
        }

        Assets.init(imagesMap,hqImages,lqImage);
    }


    /**
     * sets the image above the progress bar
     * @param bitmap transparency is allowed
     */
    public void setImage(Bitmap bitmap) {
        image.setImageBitmap(bitmap);
    }
}
