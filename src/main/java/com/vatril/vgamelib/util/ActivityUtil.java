package com.vatril.vgamelib.util;

import android.app.ActionBar;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.AndroidRuntimeException;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.vatril.vgamelib.Strings;

/**
 * Created by vatril on 07.11.16.
 */

public final class ActivityUtil {


    private ActivityUtil() {
    }

    /**
     * Sets up an @param activity for fullscreen mode.
     * Tries to hide all bars.
     * Should be called before adding content.
     *
     * @param activity the activity to set up
     * @return true is successful
     */
    public static boolean setUpFullScreenActivity(Activity activity){

        if (Build.VERSION.SDK_INT < 16) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }else {
            View decorView = activity.getWindow().getDecorView();
            try {
                activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                activity.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            }catch (AndroidRuntimeException are){
                System.err.println(Strings.VGLError + ": Could not remove Action Bar. SetUpFullScreenActivity should be called before adding content. Now attempting to hide it.");
            }
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
            ActionBar actionBar = activity.getActionBar();
            activity.setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            activity.getWindow().getAttributes().screenBrightness = 1F;
            if (actionBar != null)
            actionBar.hide();
        }
        return true;
    }

}
