package com.vatril.vgamelib;

/**
 * Created by vatril on 11.11.16.
 */


/**
 * The Gamemanager holds values that are used throughout the Game
 */
public final class Gamemanager {

    private static boolean isDebugging;
    private static double fps = 30;


    private Gamemanager() {

    }


    /**
     * Toggles debug-mode
     * @param isDebugging
     */
    public static void setIsDebugging(boolean isDebugging) {
        Gamemanager.isDebugging = isDebugging;
    }

    /**
     *
     * @return if the Game is in debug-mode
     */
    public static boolean isDebugging() {
        return isDebugging;
    }

    /**
     *
     * @return the desirable fps
     */
    public static double getFPS(){
        return fps;
    }

    /**
     * sets the desirable fps
     * @param fps
     */
    public static void setFPS(int fps){
        Gamemanager.fps = fps;
    }


}


