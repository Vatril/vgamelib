package com.vatril.vgamelib.assets;

import android.graphics.Bitmap;

import java.util.HashMap;

/**
 * Created by vatril on 09.11.16.
 */


/**
 * The assets class holds all Images as Bitmaps
 */
public class Assets {
    public static HashMap<String,Bitmap> images;
    private static int[] lasthqImages,lastlqImages;


    /**
     * Sets all maps
     *
     * @param images
     * @param hqImages
     * @param lqImages
     */
    public static void init(HashMap<String,Bitmap> images,int[] hqImages,int[] lqImages){
        lastlqImages = lqImages;
        lasthqImages = hqImages;
        Assets.images = images;
    }

    public static int[][] getLastImages() {
        return new int[][]{lastlqImages,lasthqImages};
    }

}
