//
// Created by vatril.
//

#include <jni.h>
#include <android/bitmap.h>


JNIEXPORT void JNICALL
Java_com_vatril_vgamelib_game_Renderer_fillBitmap(JNIEnv *env, jclass type, jobject bitmap, jintArray colors) {
    int* pixels;


    AndroidBitmap_lockPixels(env,bitmap, (void**)&pixels);

    jint *clrs = (*env)->GetIntArrayElements(env,colors,0);

    jint len = (*env) -> GetArrayLength(env,colors);

    int i = 0;
    for (i = 0;i < len;i++){
        pixels[i] = ((clrs[i]) & 0xff000000)|((clrs[i]>>16) & 0x000000ff)|((clrs[i]) & 0x0000ff00)|((clrs[i]<<16) & 0x00ff0000);
    }

    AndroidBitmap_unlockPixels(env, bitmap);
}